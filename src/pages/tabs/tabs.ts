import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';

import { HomePage } from '../home/home';
import { HistoryPage } from '../history/history';
import { SettingPage } from '../setting/setting';
import { ScanPage } from '../scan/scan';


@Component({
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = HistoryPage;
  tab3Root = SettingPage;

  constructor(public statusBar: StatusBar) {
    this.statusBar.styleDefault();
  }

}
