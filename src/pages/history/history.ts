import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  // 1: Year, 2: Month, 3: Week
  setView = 3;

  // total data
  usageDays = 0;
  usageHours = 0;
  usageMinutes = 0;

  // chart.js array
  labels: any[];
  usageTimeData: any[];
  goodPostureData: any[];
  badPostureData: any[];

  timer = new Date();
  yearlyCount = 12;
  monthlyCount = (new Date(this.timer.getFullYear(), this.timer.getMonth()+1, 0)).getDate();
  weeklyCount = 7;

  goodPostureHistory = [];
  badPostureHistory = [];
  state;

  goodPostureAverage = 50;
  badPostureAverage = 50;

  // chart.js property
  type;
  data;
  options;

  typePie;
  dataPie;
  optionsPie;

  constructor(public storage: Storage) {
  }

  // 페이지 활성화 때 한 번만 호출
  ionViewDidLoad() {
    this.iconClick(this.setView);
  }

  // 캐시 되있든 안되어있든 페이시 활성화 되었을 때 호출됨
  // 차트의 애니메이션을 위해 작성함
  ionViewDidEnter() {
    this.getData();
    this.setChart();
    this.setChartPie();
  }

  getData() {
    var length = 7;
    var count;
    var sum = 0;
    for(count=0; count<length; count++) {
      sum += this.goodPostureData[count];
    }
    this.goodPostureAverage = parseFloat((sum / length).toFixed(1));
    this.badPostureAverage = parseFloat((100 - this.goodPostureAverage).toFixed(1));

  }

  setChart() {
    this.usageDays = 0;
    this.usageHours = 0;
    this.usageMinutes = 0;
    var sum = 0;
    for(var i=0; i<this.usageTimeData.length; i++) {
      sum += this.usageTimeData[i];
    }

    this.usageMinutes = sum;
    while(this.usageMinutes > 59) {
      if(this.usageMinutes > 59) {
        this.usageHours++;
        this.usageMinutes = parseInt((this.usageMinutes/60).toFixed(0));
        if(this.usageHours > 23) {
          this.usageDays++;
          this.usageHours = parseInt((this.usageHours/24).toFixed(0));
        }
      }
    }

    this.type = 'bar';
    this.data = {
      labels: this.labels,
      datasets: [
        {
          type: 'line',
          label: "좋은 자세 비율",
          data: this.goodPostureData,
          pointBackgroundColor: '#2AB4A3',
          borderColor: '#2AB4A3',
          backgroundColor: 'rgba(0, 0, 0, 0)'
        }, {
          type: 'line',
          label: "나쁜 자세 비율",
          data: this.badPostureData,
          pointBackgroundColor: '#e84547',
          borderColor: '#e84547',
          backgroundColor: 'rgba(0, 0, 0, 0)'
        }
      ]
    };
    this.options = {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: 'bottom',
        labels: {
          fontFamily: "AritaSans",
          boxWidth: 15,
        },
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false,
          }
        }],
        yAxes: [{
          gridLines: {
            display: false
          }
        } ]
      }
    };
  }

  setChartPie() {
    this.typePie = 'doughnut',
    this.dataPie = {
      datasets: [{
        data: [this.goodPostureAverage, this.badPostureAverage],
        backgroundColor: [
          '#2AB4A3', '#e84547'
        ]
      }],
      labels: [
        '좋은 자세',
        '나쁜 자세'
      ]
    };
    this.optionsPie = {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: 'right',
        labels: {
          fontFamily: "AritaSans",
          boxWidth: 15,
          stretch: "center"
        },
      }
    };
  }

  // ngIf
  iconClick(viewNum) {
    if(viewNum == 1) {
      this.setView = 1;
      this.labels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
      this.usageTimeData = [10000, 8776, 9978, 7877, 13211, 7876, 6546, 9879, 6546, 8798, 12111, 6665];
      this.goodPostureData = [80, 71, 83, 69, 71, 80, 91, 83, 69, 71, 80, 91];
      this.badPostureData = [20, 29, 17, 31, 29, 20, 9, 17, 31, 29, 20, 9];
      this.ionViewDidEnter();
    } else if(viewNum == 2) {
      this.setView = 2;
      for(var i=0; i<this.monthlyCount; i++) {
        this.labels[i] = i+1;
      }
      this.usageTimeData = [340, 240, 256, 344, 412, 41, 29, 278, 336, 411, 234, 412, 41, 29, 431, 231, 121, 421, 323, 41, 29];
      this.goodPostureData = [80, 71, 83, 69, 71, 80, 91];
      this.badPostureData = [20, 29, 17, 31, 29, 20, 9];
      this.ionViewDidEnter();
    } else {
      this.setView = 3;
      this.labels = ["월", "화", "수", "목", "금", "토", "일"];
      this.usageTimeData = [340, 240, 256, 344, 412, 41, 29];
      this.goodPostureData = [80, 71, 83, 69, 71, 80, 91];
      this.badPostureData = [20, 29, 17, 31, 29, 20, 9];
      this.ionViewDidEnter();
    }
  }
}
