import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { TodayUsagePage } from '../today-usage/today-usage';
import { ConnectBlePage } from '../connect-ble/connect-ble';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';

import { BLE } from '@ionic-native/ble';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  baseUUID                       : string = "-B5A3-F393-E0A9-E50E24DCCA9E";
  moreMatServiceUUID             : string = "6E400001" + this.baseUUID;
  moreMatWriteCharacteristicUUID : string = "6E400002" + this.baseUUID;
  moreMatNotifyCharacteristicUUID: string = "6E400003" + this.baseUUID;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public statusBar: StatusBar, public ble: BLE, public zone: NgZone, public http: Http, public storage: Storage) {
    this.statusBar.styleBlackOpaque();
  }

  ionViewWillEnter() {
    this.storage.get('scanSuccess').then((data) => {
      if (data == true) {
        this.navCtrl.push(TodayUsagePage);
      } else {
        
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
  findDevice() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: '스캔 중...',
      enableBackdropDismiss: true
    });
    loading.present();

    this.ble.startScan([this.moreMatServiceUUID]).subscribe((device) => {
      this.zone.run(() => {
        this.ble.stopScan();
        loading.dismiss();
        let modal = this.modalCtrl.create(ConnectBlePage, {
          deviceName: device.name,
          deviceId: device.id
        });
        modal.present();
      });
    }, (error) => {
      console.log(error);
    });
  }

  skip() {
    this.navCtrl.push(TodayUsagePage);
  }

}
