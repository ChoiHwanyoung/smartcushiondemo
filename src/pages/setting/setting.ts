import { Component } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { BatteryStatus } from '@ionic-native/battery-status';
import { Http, Headers, RequestOptions } from '@angular/http';

import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  userName: string;
  userEmail: string;
  deviceName: string;
  
  statusLevel: number;

  getUpDefault: string = "사용안함";
  postureDefault: string = "사용안함";

  constructor(public alertCtrl: AlertController, public storage: Storage, private batterStatus: BatteryStatus, public http: Http, public navCtrl: NavController) {
    this.storage.get('deviceName').then((deviceName) => {
      this.deviceName = deviceName;
      this.getUserInfo();
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
    this.getBatteryStatus;
  }

  setImage() {

  }

  getUserInfo() {
    this.storage.get('email').then((data) => {
      this.userEmail = data;
    });
    this.storage.get('name').then((data) => {
      this.userName = data;
    });
  }

  edit() {
    let alert = this.alertCtrl.create({
      title: "edit name",
      inputs: [
        {
          name: "name",
          placeholder: "input name"
        }
      ],
      buttons: [
        {
          text: 'OK',
          handler: (inputText) => {
            this.storage.set('userName', inputText);
            this.storage.get('userName').then((userName) => {
              this.userName = userName;
            });
          }
        }
      ]
    });

    alert.present();
  }

  getBatteryStatus() {
    this.batterStatus.onChange().subscribe(status => {
      this.statusLevel = status.level;
      console.log(status.level, status.isPlugged);
    });
    this.getBatteryStatus();
  }

  getUpEvery(): string[] {
    return [
      "사용안함",
      "30분",
      "1시간",
      "2시간"
    ];
  }

  posture(): string[] {
    return [
      "사용안함",
      "사용함"
    ];
  }

  delete() {
    this.storage.clear();

    this.http.delete('http://18.218.41.18:80/delete/');
  }

  logout() {
    this.http.get('http://18.218.41.18:80/logout').map(res => res.json()).subscribe((data) => {
      var status = data.status;
      console.log(status);
      var logout: Boolean = false;

      if(status == "fail") {

      } else if(status == "success") {
        alert("로그아웃 되었습니다.");
        logout = true;
        console.log(data.status);
        console.log(data.session);

        this.navCtrl.setRoot(TabsPage).then(() => {
          this.navCtrl.popTo(LoginPage);
        }).catch((err) => {
          console.log(err);
        });
      }
    }, (err) => {
      console.log(err);
    });
  }

}
