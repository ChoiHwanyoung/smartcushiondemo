import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ChartComponent } from 'angular2-chartjs';
import { BLE } from '@ionic-native/ble';
import { Http, Headers, RequestOptions } from '@angular/http';

import h337 from "heatmap.js";

@Component({
  selector: 'page-today-usage',
  templateUrl: 'today-usage.html',
})
export class TodayUsagePage {
  @ViewChild(ChartComponent) chart: ChartComponent;

  url = "http://18.218.41.18:80";
  headers = new Headers({ "Content-Type": "application/json" });
  options = new RequestOptions({ headers: this.headers });

  deviceId                       : string = "7A335C36-CB34-F692-EE39-6E8861C71BB4";
  baseUUID                       : string = "-B5A3-F393-E0A9-E50E24DCCA9E";
  moreMatServiceUUID             : string = "6E400001" + this.baseUUID;
  moreMatWriteCharacteristicUUID : string = "6E400002" + this.baseUUID;
  moreMatNotifyCharacteristicUUID: string = "6E400003" + this.baseUUID;

  bufArray: any[] = [];
  bufLength: number = 0;
  maxBufLength: number = 486;

  timer = new Date();
  checkHours;
  chartXValue = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22];
  goodPostureHistory = [null, null, null, 71, 76, 75, 81, 73, 68, 70, null, null];
  badPostureHistory = [null, null, null, 29, 24, 25, 19, 27, 32, 30, null, null];

  // ngIf, 1=heart 2=condition 3=division 4=heatmap
  setView: number = 2;

  // vital variables(unused)
  bpm: number = 77;
  respiration: number = 15;

  // condition variables
  datas: any[] = [];
  section: any[] = [];
  // stack 3col
  col1: number[] = [];
  col2: number[] = [];
  col3: number[] = [];

  currentSeconds = 0;
  currentMinutes = 0;
  currentHours = 0;

  state = 2;
  goodCount = 0;
  badCount = 0;
  sumCount = 0;
  stateImage: string;

  postureStateMain: string = "미사용";
  posturePercentage: number = 0;
  badPosturePercentage: number = 0;
  postureMsg: string = "미사용";

  // heatmap
  heatmapInstance: any;
  tempDatas: any[] = [];
  points: any[] = [];
  sensorCol = 20;
  sensorRow = 24;
  colorLevel = 1.4;

  uid;
  data;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public zone: NgZone, public ble: BLE, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('today usage page');
    this.connCheck();
    
    this.storage.get('uid').then(data => {
      this.uid = data;
    });
    this.data = {
      uid: this.uid,
      record_time: this.yyyymmddhhmmss(),
      percentage: this.posturePercentage
    }
  }

  // 탭 할 때마다 애니메이션 출력 위함
  ionViewDidEnter() {
    this.setChart();
  }

  connCheck() {
    console.log('connCheck call');
    console.log(this.deviceId);
    this.startNotification();
    this.write();
    this.setTimes();
    this.setHeatmapInstance();
  }

  setHeatmapInstance() {
    console.log('히트맵 인스턴스 생성');
    this.heatmapInstance = h337.create({
      container: document.querySelector('#heatmap'),
      radius: 22,
      blur: .65,
      minOpacity: 1
    });
    console.log(this.heatmapInstance);
  }

  setChart() {
    this.chart.type = 'line';
    this.chart.data = {
      labels: this.chartXValue,
      datasets: [
        {
          label: "좋은 자세 비율",
          data: this.goodPostureHistory,
          pointBackgroundColor: '#2AB4A3',
          borderColor: '#2AB4A3',
          backgroundColor: 'rgba(0, 0, 0, 0)'
        }, {
          label: "나쁜 자세 비율",
          data: this.badPostureHistory,
          pointBackgroundColor: '#e84547',
          borderColor: '#e84547',
          backgroundColor: 'rgba(0, 0, 0, 0)'
        }
      ]
    };
    this.chart.options = {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: 'top',
        labels: {
          fontFamily: "AritaSans",
          boxWidth: 15,
        },
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false
          }
        }],
        yAxes: [{
          gridLines: {
            display: false
          }
        }]
      }
    };
  }

  setTimes() {
    setInterval(() => {
      if(this.state != 2) {
        this.currentSeconds++;
        if(this.currentSeconds == 60) {
          this.currentMinutes++;
          this.currentSeconds = 0;
          if(this.currentMinutes == 59) {
            this.currentHours++;
            this.currentMinutes = 0;
          }
        }
      }
    }, 1000);
  }

  getDatas() {
    if(((this.bufArray[0]&&this.bufArray[1])==0xff)&&(this.bufArray[2]==0x01)&&(this.bufArray[3]==0x10)) {
      console.log("start packet pass");
      if((this.bufArray[this.bufArray.length-2]&&this.bufArray[this.bufArray.length-1])==0xfe) {
        console.log("end packet pass");

        this.zone.run(()=> {
          this.datas.length = 0;
          this.section.length = 0;
          this.datas = this.bufArray.slice(4, this.bufArray.length-2);
          this.bufArray.length = 0;
          
          this.splitSection();
          this.sectionSetting();
          this.determinationPosture();

          console.log("section datas => " + this.section);
          console.log("sensor datas => " + this.datas);

          if(this.state != 2) {
            this.http.post(this.url + "/add-record", this.data, this.options).map(res => res.json()).subscribe(result => {
              console.log("write record...");
              console.log("record_time: " + result.record_time);
              console.log("percentage: " + result.percentage);
            }, err => {
              console.log("write record fail!" + err);
            });
          }
        });
      } else {
        console.log("end packet fail");
      }
    } else {
      console.log("start pacekt fail");
    }
    this.write();
  }

  pad2(n) {
    return (n < 10 ? '0':'') + n;
  }

  yyyymmddhhmmss() {
    var now = new Date();
    return '' + now.getFullYear() + this.pad2(now.getMonth() + 1) + this.pad2(now.getDate()) + this.pad2(now.getHours()) + this.pad2(now.getMinutes()) + this.pad2(now.getSeconds());
  }

  initVariables() {
    this.bufArray.length = 0;
    this.datas.length = 0;
    this.section.length = 0;
    this.points.length = 0;
    this.tempDatas.length = 0;
  }

  splitSection() {
    var pushCount = 0;
    var stackNum = 1;
    var popSum = 0;
    var j;

    for(var i=0; i<this.datas.length; i++) {
      if(stackNum == 1) {
        this.col1.push(this.datas[i]);
        pushCount++;
        if(pushCount == 8) {
          pushCount = 0;
          stackNum = 2;
        }
        if(this.col1.length == 32) {
          for(j=0; j<this.col1.length; j++) {
            popSum += this.col1.pop();
          }
          this.section.push(parseInt(Number(popSum/5).toFixed(0)));
          this.col1.length = 0;
          popSum = 0;
        }
      } 
        
      else if(stackNum == 2) {
        this.col2.push(this.datas[i]);
        pushCount++;
        if(pushCount == 8) {
           pushCount = 0;
          stackNum = 3;
        }
        if(this.col2.length == 32) {
          for(j=0; j<this.col2.length; j++) {
            popSum += this.col2.pop();
          }
          this.section.push(parseInt(Number(popSum/5).toFixed(0)));
          this.col2.length = 0;
          popSum = 0;
        }
      }

      else if(stackNum == 3) {
        this.col3.push(this.datas[i]);
        pushCount++;
        if(pushCount == 8) {
          pushCount = 0;
          stackNum = 1;
        }
        if(this.col3.length == 32) {
          for(j=0; j<this.col3.length; j++) {
            popSum += this.col3.pop();
          }
          this.section.push(parseInt(Number(popSum/5).toFixed(0)));
          this.col3.length = 0;
          popSum = 0;
        }
      }
    }
  }

  sectionSetting() {
    // 센서의 구조상 사용자가 편히 보려면 배열을 뒤집어야 함
    this.section.reverse();
      
    // 센서 값 보정, 센서가 방석용이 아니라 넓음 그래서 앉으면 중앙값이 크게 나옴
    this.section[10] = parseInt(Number(this.section[10]/2).toFixed(0));
    this.section[9] = this.section[9]*2;
    this.section[11] = this.section[11]*2;
  }

  determinationPosture() {
    var leftFront = this.section[0] + this.section[1] + this.section[3] + this.section[4] + this.section[6] + this.section[7];
    var rightFront = this.section[1] + this.section[2] + this.section[4] + this.section[5] + this.section[7] + this.section[8];
    var leftBack = this.section[6] + this.section[7] + this.section[9] + this.section[10] + this.section[12] + this.section[13];
    var rightBack = this.section[7] + this.section[8] + this.section[10] + this.section[11] + this.section[13] + this.section[14];

    this.storage.set('state', this.state);

    if(leftFront+rightFront+leftBack+rightBack == 0) {
      this.postureMsg = "미사용";
      this.stateImage = "";
      this.state = 2;
    } else if((leftFront+leftBack) - (rightFront+rightBack) > 60) {
      this.postureMsg = "왼쪽 치우침";
      this.stateImage = "lefthigh";
      this.state = 0;
    } else if((rightFront+rightBack) - (leftFront+leftBack) > 60) {
      this.postureMsg = "오른쪽 치우침";
      this.stateImage = "righthigh";
      this.state = 0;
    } else if(leftFront+rightFront > leftBack+rightBack) {
      this.postureMsg = "앞쪽 치우침";
      this.stateImage = "fronthigh";
      this.state = 0;
    } else {
      this.postureMsg = "좋음";
      this.stateImage = "good";
      this.state = 1;
    }

    // document
    var greenRedSwitch = document.getElementById('greenRed');
    greenRedSwitch.style.color = 'gray';
    
    if(this.state == 1) {
      this.goodCount++;
      this.postureStateMain = "좋음";
      greenRedSwitch.style.color = "#2AB4A3";
    } else if(this.state == 0) {
      this.badCount++;
      this.postureStateMain = "나쁨";
      greenRedSwitch.style.color = '#e84547';
    } else {
      this.postureStateMain = "미사용";
      greenRedSwitch.style.color = 'gray';
    }
    
    this.sumCount = this.goodCount + this.badCount;
    if(this.badCount != 0) {
      this.posturePercentage = parseInt(((this.goodCount/this.sumCount) * 100).toFixed(0));
      this.badPosturePercentage = 100 - this.posturePercentage;
    } else {
      this.posturePercentage = 100;
      this.badPosturePercentage = 100 - this.posturePercentage;
    }
  }

  // heatmapView() {
  //   this.tempDatas.length = 0;
  //   this.points.length = 0;
  //   var index = 0;

  //   for(var i=0; i<this.sensorCol; i++) {       // 데이터만 추출 저장, [20][24], index 초기값 0 , 24씩 더함
  //     this.tempDatas[i] = this.datas.slice(index, index + this.sensorRow);
  //     index += this.sensorRow;
  //   }
  //   for(var col=0; col<this.sensorCol; col++) {
  //     for(var row=0; row<this.sensorRow; row++) {
  //       var dataPoint = {
  //         x: row * 15,
  //         y: col * 15,
  //         value: this.tempDatas[col][row] * this.colorLevel
  //       }
  //       this.points.push(dataPoint);
  //     }
  //   }
  //   var data = {
  //     max: 253,
  //     min: 0,
  //     data: this.points
  //   }
  //   this.heatmapInstance.setData(data);
  //   this.heatmapInstance.repaint();
  // }

  // 그리드 형태의 히트맵 색상 변화용
  heatMapColorforValue(value): string {
    if(value > 248) {
      value = 248;
    }
    var percentPostureValue = 248 - value;
    
    return "rgb(255, " + percentPostureValue + "," + percentPostureValue + ")";
  }

  // ngIf
  iconClick(viewNum) {
    if(viewNum == 1) {
      this.setView = 1;
    }  if(viewNum == 2) {
      this.setView = 2;
    }  if(viewNum == 3) {
      this.setView = 3;
    } if(viewNum == 4) {
      this.setView = 4;
      // this.heatmapView();
    }
  }

  // BLE connect
  startNotification() {
    this.ble.startNotification(this.deviceId, this.moreMatServiceUUID, this.moreMatNotifyCharacteristicUUID).map(res => new Uint8Array(res)).subscribe((buf) => {
      for(var i=0; i<buf.length; i++) {
        this.bufArray.push(buf[i]);
      }

      this.bufLength += buf.length;
      if(this.bufLength == this.maxBufLength) {         /* 486bytes 받았으면, 초기화 및 보여줌 */
        this.bufLength = 0;
        this.getDatas();
      }
    }, (err) => {
      console.log(err);
    });
  }

  write() {
    this.ble.write(this.deviceId, this.moreMatServiceUUID, this.moreMatWriteCharacteristicUUID, this.packet()).then((msg) => {
      console.log("write success");
    }, (error) => {
      console.log("write fail");
    });
    // this.initVariables();
  }

  packet() {
    var data = new Uint8Array(6);
    data[0] = 0xFF;
    data[1] = 0xFF;
    data[2] = 0x01;
    data[3] = 0x10;
    data[4] = 0xFE;
    data[5] = 0xFE;
    return data.buffer;
  }
}
