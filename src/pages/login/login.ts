import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  // 1: login, 2: register
  setView = 1;

  url = "http://18.218.41.18:80";

  loginEmail: string = "test2@test.com";
  loginPassword: string = "Rksekdi159!";

  email: string = "";
  password: string = "";
  rePassword: string = "";
  name: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public storage: Storage, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: '로그인 중...',
      enableBackdropDismiss: true
    });
    loading.present();
    if(this.loginEmail=="" || this.loginPassword=="") {
      loading.dismiss();
      alert("입력하지 않은 정보가 있습니다.");
      return false;
    } else {
      var data = {
        email: this.loginEmail,
        password: this.loginPassword
      };
      var headers = new Headers({ "Content-Type": "application/json" });
      var options = new RequestOptions({ headers: headers });

      this.http.post(this.url + "/login", data, options).map(res => res.json()).subscribe(data => {
        this.loginEmail = "";
        this.loginPassword = "";

        var status = data.status;
        console.log(status);

        if(status == "1") {
          loading.dismiss();
          alert("아이디를 확인해주세요.")
          return false;
        } else if(status == "2") {
          loading.dismiss();
          alert("비밀번호를 확인해주세요.")
          return false;
        } else {
          var uid = data.uid;
          console.log(uid);
          this.storage.set('uid', uid);
          console.log(data.session);

          this.http.get(this.url + "/login/" + uid).map(res => res.json()).subscribe((data) => {
            var storageEmail = data.email;
            var storageName = data.name;
            console.log(data.session);
            
            console.log(storageEmail, storageName);
            this.storage.set('email', storageEmail);
            this.storage.set('name', storageName);

            loading.dismiss();
            alert(this.loginEmail + "로그인 되었습니다.");
            this.navCtrl.push(TabsPage);
          }, (err) => {
            console.log(err);
          });
        }
      }, err => {
        alert("post요청실패: /login" + this.loginEmail + this.loginPassword);
        console.log(err);
      });
    }
  }
  
  registerPage() {
    this.setView = 2;
  }

  cancel() {
    this.setView = 1;
    this.email = "";
    this.password = "";
    this.rePassword = "";
    this.name = "";
  }

  register() {
    var regEmail = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
    var regName = /^[0-9a-zA-Z]/i;
    var regPassword = /^(?=.*[a-zA-Z])((?=.*\d)|(?=.*\W)).{8,20}$/;
    
    if(this.email=="" || this.password=="" || this.rePassword=="" || this.name=="") {
      alert("입력하지 않은 정보가 있습니다.");
      return false;
    } else {
      if(this.email.match(regEmail) == null) {
        alert("올바르지 않은 이메일 형식입니다.");
        return false;
      } else {
        if(this.password != this.rePassword) {
          alert("비밀번호를 재 확인해주세요.");
          return false;
        } else {
          if(this.name.match(regName) == null) {
            alert("이름은 영어와 숫자만 입력 가능합니다.");
            return false;
          } else { 
            if(this.password.match(regPassword) == null) {
              alert("비밀번호 형식과 맞지 않습니다.");
              return false;
            } else {
              /////////////////////////////////////////////////////////////////// 성공
              var data = {
                email: this.email,
                password: this.password,
                name: this.name
              };
              var headers = new Headers({ "Content-Type": "application/json" });
              var options = new RequestOptions({ headers: headers });

              this.http.post(this.url + "/register", data, options).map(res => res.json()).subscribe(data => {
                console.log("login info: " + data);
                alert(this.name + "님 가입 성공");
                this.cancel();
                this.email = data.email;
                this.password = data.email;
              }, err => {
                console.log(err);
                alert("중복된 이메일이 존재합니다.");
              });
            }
          }
        }
      }
    }
  }

  guest() {
    this.navCtrl.push(TabsPage);
  }
}
