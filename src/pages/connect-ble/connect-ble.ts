import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';
import { Storage } from '@ionic/storage';

import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-connect-ble',
  templateUrl: 'connect-ble.html',
})
export class ConnectBlePage {
  deviceName = this.navParams.get('deviceName');
  deviceId = this.navParams.get('deviceId');

  heatmapInstance                : any;                /* 히트맵 */
  points                         : any[] = [];

  colorLevel                     : number = 1.3;       /* 색상 레벨 조절 */
  maxPressure                    : number = 0;         /* 순간 최고 압력 */
  count                          : number = 0;         /* 486bytes 받은 횟수 */
  maxCount                       : number = 0;         /* 초당 몇회인지 */
  kbs                            : number = 0;         /* 전송속도(kb) */

  datas                          : any[] = [];         /* 데이터 480bytes */
  index                          : number = 4;         /* 데이터 시작 인덱스 */
  bufArray                       : any[] = [];         /* 패킷 486bytes */
  bufLength                      : number = 0;         /* 버퍼길이 체크용도 */
  maxBufLength                   : number = 486;       /* 최대 버퍼길이 */
  sensorCol                      : number = 20;
  sensorRow                      : number = 24;

  currentSeconds                 : number = 0;         /* 블루투스 연결시간 */
  currentMinutes                 : number = 0;
  currentHours                   : number = 0;
  

  constructor(public navCtrl: NavController, public navParams: NavParams, public ble: BLE, public zone: NgZone, public storage: Storage) {
    console.log(this.deviceName);
    console.log(this.deviceId);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConnectBlePage');
    this.storage.set('deviceName', this.deviceName);
  }

  connect() {
    this.ble.connect(this.deviceId).subscribe((device) => {
      console.log('connection success');
      this.storage.set('deviceId', this.deviceId);
      this.storage.set('scanSuccess', true);
      this.navCtrl.push(TabsPage);
    }, (error) => {
      // 연결 끊어지면 세팅에서 연결기기 이름 없애기 위해
      this.storage.remove('deviceName');
      this.storage.remove('scanSuccess');
      console.log('connection fail');
    })
  }

}
