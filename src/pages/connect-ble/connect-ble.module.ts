import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConnectBlePage } from './connect-ble';
import { ScanPage } from '../scan/scan';

@NgModule({
  declarations: [
    ConnectBlePage,
  ],
  imports: [
    IonicPageModule.forChild(ConnectBlePage),
    ScanPage
  ],
})
export class ConnectBlePageModule {}

