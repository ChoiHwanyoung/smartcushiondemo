import { Component, NgZone, AfterViewInit } from '@angular/core';
import { BLE } from '@ionic-native/ble';
import h337 from "heatmap.js";

@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html'
})

export class ScanPage implements AfterViewInit {
  
  heatmapInstance                   : any;                /* 히트맵 */
  points                            : any[] = [];

  colorLevel                        : number = 1.5;       /* 색상 레벨 조절 */
  maxPressure                       : number = 0;         /* 순간 최고 압력 */
  count                             : number = 0;         /* 486bytes 받은 횟수 */
  maxCount                          : number = 0;         /* 초당 몇회인지 */
  kbs                               : number = 0;         /* 전송속도(kb) */

  datas                             : any[] = [];         /* 데이터 480bytes */
  index                             : number = 4;         /* 데이터 시작 인덱스 */
  bufArray                          : any[] = [];         /* 패킷 486bytes */
  bufLength                         : number = 0;         /* 버퍼길이 체크용도 */
  maxBufLength                      : number = 486;       /* 최대 버퍼길이 */
  sensorCol                         : number = 20;
  sensorRow                         : number = 24;

  currentSeconds                    : number = 0;         /* 블루투스 연결시간 */
  currentMinutes                    : number = 0;
  currentHours                      : number = 0;

  connectDevice                     : string = "disconnection";
  deviceList                        : any[];
  deviceId                          : string;
  baseUUID                          : string = "-B5A3-F393-E0A9-E50E24DCCA9E";
  moreMatServiceUUID                : string = "6E400001" + this.baseUUID;
  moreMatWriteCharacteristicUUID    : string = "6E400002" + this.baseUUID;
  moreMatNotifyCharacteristicUUID   : string = "6E400003" + this.baseUUID;

  constructor(private zone: NgZone, public ble: BLE) {
    this.ble.startStateNotifications().subscribe((msg) => {
      if(msg == "off") {
        alert("Bluetooth off");
      }
    });
  }

  ngAfterViewInit() {              /* 히트맵 인스턴스 초기화 */
    this.heatmapInstance = h337.create({
      container: document.querySelector('#heatmap'),
      radius: 22,
      blur: .65,
      minOpacity: 1
    });
  }

  scan() {                         /* 스캔 */
    this.deviceList = [];

    this.ble.startScan([this.moreMatServiceUUID]).subscribe((device) => {
      this.zone.run(() => {
        this.ble.stopScan();
        this.deviceList.push(device);
        this.connect(device.id, device.name);
      });
    }, (error) => {
      console.log(error);
    });
  }

  connect(deviceId, deviceName) {
    this.ble.connect(deviceId).subscribe((device) => {
      console.log('connection success');

      this.connectDevice = "connection: " + deviceName;
      this.deviceId = deviceId;
      this.deviceList = [];
      this.startNotification();

      var timer = setInterval(() => {         /* 타이머 시작 */
        this.stopwatch();
        this.maxCount = this.count;
        this.count = 0;
        this.kbs = this.maxBufLength * this.maxCount / 1000;
      }, 1000);

      if(!this.ble.isConnected) {
        clearInterval(timer);
      }

      this.write();
    }, (error) => {
      console.log('connection fail');
      this.ble.disconnect(this.deviceId);
      this.scan();
    });
  }

  startNotification() {
    this.ble.startNotification(this.deviceId, this.moreMatServiceUUID, this.moreMatNotifyCharacteristicUUID).subscribe((buffer) => {
      var buf = new Uint8Array(buffer);

      for(var i=0; i<buf.length; i++) {
        this.bufArray.push(buf[i]);
      }

      this.bufLength += buf.length;
      if(this.bufLength == this.maxBufLength) {         /* 486bytes 받았으면, 초기화 및 보여줌 */
        this.count++;
        this.bufLength = 0;
        this.view();
      }
    });
  }

  view() {
    if(((this.bufArray[0]&&this.bufArray[1])==0xff)&&(this.bufArray[2]==0x01)&&(this.bufArray[3]==0x10)) {
      console.log("start packet pass");
      if((this.bufArray[this.bufArray.length-2]&&this.bufArray[this.bufArray.length-1])==0xfe) {
        console.log("end packet pass");

        this.zone.run(()=> {
          this.maxPressure = Math.max.apply(Math, this.bufArray.slice(4, this.bufArray.length-2));

          console.log("data:" + this.bufArray.slice(4, this.bufArray.length-2));

          for(var i=0; i<this.sensorCol; i++) {       // 데이터만 추출 저장, [20][24], index 초기값 4 , 24씩 더함
            this.datas[i] = this.bufArray.slice(this.index, this.index + this.sensorRow);
            this.index += this.sensorRow;
          }
          for(var col=0; col<this.sensorCol; col++) {
            for(var row=0; row<this.sensorRow; row++) {
              var dataPoint = {
                x: row * 15,
                y: col * 15,
                value: this.datas[col][row] * this.colorLevel
              }
              this.points.push(dataPoint);
            }
          }
          var data = {
            max: 253,
            min: 0,
            data: this.points
          }

          console.log("포인트1");
          console.log(this.points);
          console.log("데이터1");
          console.log(data);
          this.heatmapInstance.setData(data);
          console.log("repaint 호출");
          this.heatmapInstance.repaint();
        });
      } else {
        console.log("end packet fail");
      }
    } else {
      console.log("start pacekt fail");
    }
    this.write();
  }

  write() {
    this.ble.write(this.deviceId, this.moreMatServiceUUID, this.moreMatWriteCharacteristicUUID, this.packet()).then((msg) => {
      console.log("write success");
    }, (error) => {
      console.log("write fail");
    });

    this.init();
  }

  init() {
    this.bufArray.length = 0;       /* 초기화 */
    this.datas.length = 0;
    this.points.length = 0;
    this.index = 4;
  }

  packet() {
        var data = new Uint8Array(6);
        data[0] = 0xFF;
        data[1] = 0xFF;
        data[2] = 0x01;
        data[3] = 0x10;
        data[4] = 0xFE;
        data[5] = 0xFE;
        return data.buffer;
  }

  stopwatch() {                         /* 초 단위 타이머 */
    this.currentSeconds += 1;

    if(this.currentSeconds == 60) {     /* 초-분 */
      this.currentMinutes += this.currentSeconds / 60;
      this.currentSeconds = this.currentSeconds % 60;

      if(this.currentMinutes == 60) {   /* 분-시 */
        this.currentHours += this.currentMinutes / 60;
        this.currentMinutes = this.currentMinutes % 60;
      }
    }
  }

  /*
  heatMapColorforValue(value): string {         // 히트맵 Grid형태
    var percentPostureValue = 255 - (value * this.colorLevel);
    if(percentPostureValue < 0) {
      percentPostureValue = 0;
    }
    return "rgb(255, " + percentPostureValue + "," + percentPostureValue + ")";
  }

  heatMapHSL(value): string {
    var percentPostureValue = 253 - (value * this.colorLevel);
    if(percentPostureValue < 0) {
      percentPostureValue = 0;
    }
    return "hsl(" + percentPostureValue + ", 100%, 50%)";
  }
  */
}
