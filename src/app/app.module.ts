import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ConnectBlePage } from '../pages/connect-ble/connect-ble';

import { HistoryPage } from '../pages/history/history';
import { SettingPage } from '../pages/setting/setting';
import { TodayUsagePage } from '../pages/today-usage/today-usage';
import { ScanPage } from '../pages/scan/scan';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { BatteryStatus } from '@ionic-native/battery-status';
import { ChartModule } from 'angular2-chartjs';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    TabsPage,
    ConnectBlePage,
    HistoryPage,
    SettingPage,
    TodayUsagePage,
    ScanPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    ChartModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: ''
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    TabsPage,
    ConnectBlePage,
    HistoryPage,
    SettingPage,
    TodayUsagePage,
    ScanPage
  ],
  providers: [
    BLE,
    BatteryStatus,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
